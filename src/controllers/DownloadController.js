import express from 'express'
import archiver from 'archiver'
import transformationUtil from 'keys-translations-manager-core/lib/transformationUtil'
import Translations from '../models/TranslationModel'
import config from '../../ktm.config'
import nodeExcel from 'excel-export'
const locales = config.locales
const lenLocales = locales.length
const router = express.Router()
const document2FileContent = transformationUtil.document2FileContent

router.route('/:outputType/:fileType/:project/:locale')
		.get(function(req, res) {
			// outputType (f: format, n: none)
			// fileType (json, flat, properties)
			const { outputType, fileType, project, locale } = req.params

			let query,
				criteria = {
					"project": project
				},
				select = {
					"_id": 0,
					"key": 1
				};

			criteria[locale] = {$exists: true};
			select[locale] = 1;
			query = Translations.find(criteria).select(select);
			if (outputType === "f") {
				query.sort({'key':-1});
			}
			query.exec(function(err, translations) {
				let str,
					formatted = outputType === "f";

				if (err) {
					res.status(500).send(err);
				}

				str = document2FileContent(translations, locale, fileType, formatted);

				if (fileType === "json" || fileType === "flat") {
					res.set({
						"Content-Disposition": "attachment; filename=\"translation.json\"",
						"Content-Type": "application/json; charset=utf-8"
					});
				} else if (fileType === "properties") {
					res.set({
						"Content-Disposition": "attachment; filename=\"translation.properties\"",
						"Content-Type": "text/x-java-properties; charset=utf-8"
					});
				}
				res.send(str);

			});
		});

router.route('/:outputType/:fileType/:project')
		.get(function(req, res) {
			// outputType (f: format, n: none)
			// fileType (json, flat, properties)
			const { outputType, fileType, project } = req.params,
				archive = archiver.create('zip', {}),
				zipHandler = function(stream, locale, fileExt) {
					archive.append(stream, { name: locale + '/translation.' + fileExt });
					if (++count === lenLocales) {
						archive.finalize();
					}
				};

			let query,
				criteria = {},
				select = {
					"_id": 0,
					"key": 1
				},
				count = 0,
				locale;

			res.set({
				'Content-Type': 'application/zip',
				'Content-disposition': 'attachment; filename=translations.zip'
			});
			archive.pipe(res);

			for (let i = 0; i < lenLocales; i++) {
				locale = locales[i];

				criteria.project = project;
				select[locale] = 1;
				query = Translations.find(criteria).select(select);
				if (outputType === "f") {
					query.sort({'key':-1});
				}
				query.exec(function(err, translations) {
					let str,
						locale = this,
						formatted = outputType === "f",
						finalFileType = fileType === "flat" ? "json" : fileType;

					if (err) {
						res.status(500).send(err);
					}

					str = document2FileContent(translations, locale, fileType, formatted);
					zipHandler(str, locale, finalFileType);
				}.bind(locale));
			}
		});

router.route('/csv')
		.get(function(req, res) {
			Translations.find({}, null, {sort: {'_id': 1}}, function(err, translations) {
				const delimiter = "\t"
				let len = translations.length,
					translation,
					i,
					str;

				if (err) {
					res.status(500).send(err);
				}

				// console.log(translations);

				var conf ={};
			 //    conf.name = "export";
			 //  	conf.cols = [{
				// 	caption:'_id',
			 //        type:'string',
			 //        width: 40
				// },{
				// 	caption:'key',
			 //        type:'string',
			 //        width:40
				// }, {
				// 	caption:'en',
			 //        type:'string',
			 //        width:40
				// }, {
				// 	caption:'vi',
			 //        type:'string',
			 //        width:40
				// }, {
				// 	caption:'jp',
			 //        type:'string',
			 //        width:40
				// }];
			 //  	conf.rows = translations

			 conf.cols = [{
				caption:'_id',
		        width: 40
			},{
				caption:'key',
		        width: 40
			}, {
				caption:'en',
		        width: 40
			}, {
				caption:'vi',
		        width: 40
			}, {
				caption:'jp',
		        width: 40
			}];
		  	// conf.rows = [
		 		// ['pi', new Date(Date.UTC(2013, 4, 1)), true, 3.14],
		 		// ["e", new Date(2012, 4, 1), false, 2.7182],
		   //      ["M&M<>'", new Date(Date.UTC(2013, 6, 9)), false, 1.61803],
		   //      ["null date", null, true, 1.414]  
		  	// ];
		  	conf.rows = translations.map(translation => {
		  		return [translation._id, translation.key, translation.en || '', translation.vi || '', translation.jp || ''];
		  	});
			  	var result = nodeExcel.execute(conf);
			  	res.setHeader('Content-Type', 'application/vnd.openxmlformats');
			  	res.setHeader("Content-Disposition", "attachment; filename=" + "export.xlsx");
			  	res.end(result, 'binary');;
				res.end();
			});
		});

export default router
