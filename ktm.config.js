module.exports = {
	/**
	 * If you change the configurations,
	 * don't forget to rebuild the code (npm run build) and
	 * restart the server (npm run start).
	*/
	server: {
		hostname: 'localhost',
		port: 5000
	},
	database: 'mongodb://localhost:27017/translationdb-new',
	locales: ['en', 'vi', 'jp'],
	projects: [ // make sure the ids are 'String' type
		{id:'p1', name:'Hottab admin'}
	],
	enableNotifications: true
};
